package fr.llelievr;

import api.DebugFile;
import api.config.BlockConfig;
import api.element.block.Blocks;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.resource.BlockTexture;
import api.utils.ResourceLocation;
import org.schema.game.common.data.element.ElementInformation;

import javax.vecmath.Vector4f;

public class Main extends StarMod {

    public static void      main(String[] args) {}

    @Override
    public void onGameStart() {
        setModName("StarMod");
        setModVersion("0.1");
        setModDescription("This is a test!");

        for (int i = 0; i < 250; i++) {
            StarLoader.getResourcesRegistry().getBlocksAtlas()
                    .addBlockTexture(new ResourceLocation(this, "test_texture_" + i),
                            new BlockTexture(BlockTexture.Resolution.PIXELS128, new ResourceLocation(this, "blocks/test_texture_64.png")));
        }
//        StarLoader.getResourcesRegistry().getBlocksAtlas()
//                .addBlockTexture(new ResourceLocation(this, "test_texture"),
//                        new BlockTexture(BlockTexture.Resolution.PIXELS128, new ResourceLocation(this, "blocks/test_texture_128.png")));
    }

    @Override
    public void onEnable() {
        DebugFile.log("StarMod Loaded", this);
//        StarLoader.registerListener(ShieldCapacityCalculateEvent.class, new Listener() {
//            @Override
//            public void onEvent(Event event) {
//
//                ShieldCapacityCalculateEvent ps = (ShieldCapacityCalculateEvent)event;
//                ps.getCapacity();
//            }
//        });

    }

    @Override
    public void onBlockConfigLoad(BlockConfig config) {
        ElementInformation imp = BlockConfig.newElement("Test BLOCK", new short[]{256 * 2});
        imp.setBuildIconNum(Blocks.GREY_ADVANCED_ARMOR.getId());
        //Give it lotss of health
        imp.setMaxHitPointsE(100000);
        imp.setArmorValue(1000);
        //Make it emit light
        imp.lightSource = true;
        imp.lightSourceColor.set(new Vector4f(1F, 0F, 1F, 1F));
        //Make it activatable,
        imp.setCanActivate(true);
        config.add(imp);
    }
}
